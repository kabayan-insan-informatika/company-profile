<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function index() {
        $title = config("app.name");
        return view("welcome", compact('title'));
    }
}
