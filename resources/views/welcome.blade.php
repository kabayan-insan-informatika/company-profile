
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Diana Sena Kurnia" />

	<!-- Stylesheets-->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('style.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/swiper.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/dark.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/font-icons.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>{{ $title }}</title>
</head>
<body class="stretched">
	<div id="wrapper" class="clearfix">
		<header id="header" class="full-header transparent-header page-section dark" data-sticky-class="not-dark">
			<div id="header-wrap">
				<div class="container">
					<div class="header-row">
						<div id="logo">
							<a href="{{ route('index') }}" class="p-8 standard-logo" data-dark-logo="images/logo-dark.png"><img style="padding:10px" src="{{ asset('images/logo.png') }}" alt="Logo"></a>
							<a href="{{ route('index') }}" class="p-8 retina-logo" data-dark-logo="images/logo-dark.png"><img style="padding:10px" src="{{ asset('images/logo.png') }}" alt="Logo"></a>
						</div>
                        <div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>
						<nav class="primary-menu">
							<ul class="menu-container one-page-menu" data-easing="easeInOutExpo" data-speed="1500" style="border: 0">
								<li class="menu-item"><a class="menu-link" href="#" data-href="#home"><div>Home</div></a></li>
								<li class="menu-item"><a class="menu-link" href="#" data-href="#section-benefit"><div>Benefit</div></a></li>
								<li class="menu-item"><a class="menu-link" href="#" data-href="#section-tahapan-hiring"><div>Tahapan Hiring</div></a></li>
								<li class="menu-item"><a class="menu-link" href="#" data-href="#section-contact"><div>Contact</div></a></li>
								<li class="menu-item"><a class="menu-link" href="https://docs.google.com/forms/d/e/1FAIpQLSenqiLdSS0SEcRDd2lDI8t3soiqM0mN_ZoiQ5RM8Rxkp2MMIA/viewform?usp=sharing" target="_blank"><div>Karir</div></a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>
		</header>

		<div id="home" class="page-section" style="position:absolute;top:0;left:0;width:100%;height:200px;z-index:-2;"></div>
		<section id="slider" class="slider-element slider-parallax min-vh-60 min-vh-md-100 dark include-header" 
			style="background-image: url('{{ asset('images/slider/swiper/1.jpg?v='.time()) }}'); center center no-repeat; background-size: cover">
			<div class="slider-inner">
				<div class="vertical-middle slider-element-fade">
					<div class="container-fluid py-5" style="background-color: rgba(0, 0, 0, 0.6)">
						<div class="heading-block text-center border-bottom-0">
							<h1>Mencari Karyawan dengan mudah</h1>
							<span>Kabayan Insan Informatika membantu anda untuk menemukan talenta yang anda butuhkan dengan sangat mudah</span>
						</div>

						<div class="text-center">
							<a href="#section-contact" class="button button-3d button-xlarge mb-0"><i class="icon-star3"></i>Hubungi Kami</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="content">
			<div class="content-wrap">
				<section id="section-benefit" class="page-section">
					<div class="container clearfix">
						<div class="heading-block center">
							<h2>Kami selalu <span>SIAP</span></h2>
							<span>Percayakan kebutuhan karyawan anda kepada kami</span>
						</div>
						<div class="row justify-content-center col-mb-50">
							<div class="col-sm-6 col-lg-4">
								<div class="feature-box media-box">
									<div class="fbox-media">
										<img src="images/services/1.jpg" alt="Why choose Us?">
									</div>
									<div class="fbox-content px-0">
										<h3>Cepat</h3>
										<p>Kami memberikan bakat yang Anda butuhkan dalam waktu kurang dari 14 hari.</p>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-lg-4">
								<div class="feature-box media-box">
									<div class="fbox-media">
										<img src="images/services/2.jpg" alt="Why choose Us?">
									</div>
									<div class="fbox-content px-0">
										<h3>Akurat</h3>
										<p>Tidak hanya portofolio, Kami juga mengkurasi secara ketat semua calon talenta.</p>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-lg-4">
								<div class="feature-box media-box">
									<div class="fbox-media">
										<img src="images/services/3.jpg" alt="Why choose Us?">
									</div>
									<div class="fbox-content px-0">
										<h3>Hemat Biaya</span></h3>
										<p>Bayar hanya ketika Anda mendapatkan bakat yang Anda butuhkan</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section id="section-tahapan-hiring" class="page-section topmargin-lg">
					<div class="heading-block center">
						<h2>Hiring Process</h2>
					</div>
                    <div class="content-wrap">
                        <div class="container">
                            <div class="line d-block d-md-none"></div>
                            <ul class="process-steps row col-mb-30 justify-content-center mb-4">
                                <li class="col-sm-6 col-lg-3">
                                    <a href="#" class="i-circled i-alt mx-auto">1</a>
                                    <h5>Kontak Kami dan kirimkan lowongan yang anda perlukan beserta persyaratan yang dibutuhkan</h5>
                                </li>
                                <li class="col-sm-6 col-lg-3">
                                    <a href="#" class="i-circled i-alt mx-auto">2</a>
                                    <h5>Shortlist - Kami akan melakukan proses Seleksi untuk mendapatkan talenta terbaik sesuai kebutuhan Anda</h5>
                                </li>
                                <li class="col-sm-6 col-lg-3">
                                    <a href="#" class="i-circled i-alt mx-auto">3</a>
                                    <h5>Lakukan wawancara dengan talenta yang kami tawarkan</h5>
                                </li>
                                <li class="col-sm-6 col-lg-3">
                                    <a href="#" class="i-circled i-alt mx-auto">4</a>
                                    <h5>Proses tandatangani kesepakatan memulai kerjasama</h5>
                                </li>
                            </ul>
                        </div>
                    </div>
				</section>

				<section id="section-contact" class="page-section">
					<div class="heading-block text-center">
						<h2>Hubungi kami</h2>
						<span>Isi formulir di bawah ini untuk mendapatkan
                            penawaran dan profile perusahaan kami
                        </span>
					</div>
					<div class="container clearfix">
						<div class="row align-items-stretch col-mb-50 mb-0">
							<div class="col-lg-6">

								<div class="fancy-title title-border">
									<h3>Form Email</h3>
								</div>

								<div class="form-widget">
									<div class="form-result"></div>

									<form class="mb-0" id="template-contactform" name="template-contactform">
										<div class="form-process">
											<div class="css3-spinner">
												<div class="css3-spinner-scaler"></div>
											</div>
										</div>

										<div class="row">
											<div class="col-12 form-group">
												<label for="template-contactform-name">Nama <small>*</small></label>
												<input type="text" id="template-contactform-name" name="template-contactform-name" value="" required class="sm-form-control required" />
											</div>

											<div class="col-12 form-group">
												<label for="template-contactform-email">Email <small>*</small></label>
												<input type="email" id="template-contactform-email" name="template-contactform-email" value="" required class="required email sm-form-control" />
											</div>

											<div class="col-12 form-group">
												<label for="template-contactform-phone">Nomor WA<small>*</small></label>
												<input type="number" id="template-contactform-phone" name="template-contactform-phone" value="" required class="required sm-form-control" />
											</div>

											<div class="w-100"></div>

											<div class="col-12 form-group">
												<label for="template-contactform-message">Pesan <small>*</small></label>
												<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
											</div>

											<div class="col-12 form-group d-none">
												<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" required class="sm-form-control" />
											</div>

											<div class="col-12 form-group">
												<button class="button button-3d m-0" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Kirim Email</button>
											</div>
										</div>

										<input type="hidden" name="prefix" value="template-contactform-">

									</form>
								</div>

							</div>
                            <div class="col-lg-6 min-vh-40">
                                <div class="gmap h-100"
                                    data-latitude="-6.224090272448092"
                                    data-longitude="106.84182112437055"
                                    data-markers="[{address: &quot;The Kasablanka, RW.14, Menteng Dalam, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta&quot;, html: &quot;<div class=\&quot;p-2\&quot; style=\&quot;width: 300px;\&quot;><h4 class=\&quot;mb-2\&quot;>Hi, Kami Kabayan Insan <span>Informatika!</span></h4><p class=\&quot;mb-0\&quot; style=\&quot;font-size:1rem;\&quot;>Kami membantu  menemukan talenta terbaik sesuai dengan kebutuhan perusahaan anda</p></div>&quot;, icon:{ image: &quot;images/icons/map-icon-red.png&quot;, iconsize: [32, 39], iconanchor: [32,39] } }]"
                                    data-scrollwheel="true" style="position: relative; overflow: hidden;">
                                    <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                                        <div class="gm-err-container">
                                            <div class="gm-err-content">
                                                <div class="gm-err-icon">
                                                    <img src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" alt="" draggable="false" style="user-select: none;">
                                                </div>
                                                <div class="gm-err-title">Oops! Something went wrong.</div>
                                                <div class="gm-err-message">This page didn't load Google Maps correctly. See the JavaScript console for technical details.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
						<div class="w-100">
							<div class="row col-mb-50">
								<div class="col-sm-6 col-lg-6">
									<div class="feature-box fbox-center fbox-bg fbox-plain">
										<div class="fbox-icon">
											<a href="#"><i class="icon-map-marker2"></i></a>
										</div>
										<div class="fbox-content">
											<h3>CV Kabayan Insan Informatika<span class="subtitle">Eightyeight@Kasablanka, Tower A, 12 th Floor, Unit A&H<br><br>
												Jl. Casablanca Raya Kav.88, Desa/Kelurahan Menteng Dalam,<br>Kec. Tebet,
												Kota Adm. Jakarta Selatan, Provinsi DKI Jakarta, 12870</span></h3>
										</div>
									</div>
								</div>

								<div class="col-sm-6 col-lg-3">
									<div class="feature-box fbox-center fbox-bg fbox-plain">
										<div class="fbox-icon">
											<a href="#"><i class="icon-phone2"></i></a>
										</div>
										<div class="fbox-content">
											<h3>Whatsapp<span class="subtitle">0878-2526-8558</span><br><br><br></h3>
										</div>
									</div>
								</div>

								<div class="col-sm-6 col-lg-3">
									<div class="feature-box fbox-center fbox-bg fbox-plain">
										<div class="fbox-icon">
											<a href="#"><i class="icon-phone3"></i></a>
										</div>
										<div class="fbox-content">
											<h3>Phone<span class="subtitle">021-3000-3697<br><br><br><br></span></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</section>
        <div class="clearfix page-section">
            <div class="parallax skrollable skrollable-between" style="background-image: url({{ asset('images/parallax/3.jpg?v='.time()) }}); padding: 100px 0px;">
                <div class="heading-block center border-bottom-0 mb-0">
                    <h2 style="padding: 40px; background: rgba(255,255,255,0.6); margin-bottom: 20px;">Daftar menjadi Talent Kabayan Insan Informatika</h2>
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSenqiLdSS0SEcRDd2lDI8t3soiqM0mN_ZoiQ5RM8Rxkp2MMIA/viewform?usp=sharing" target="_blank" class="button button-3d m-0">Daftar di sini</a>
                </div>
            </div>
        </div>
		
		<footer id="footer" class="dark">
			<div id="copyrights">
				<div class="container">
					<div class="row col-mb-30">
						<div class="text-center">
							Copyrights &copy; {{ date('Y') . " " . config("app.name") }}
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<div id="gotoTop" class="icon-angle-up"></div>
	<script src="{{ asset('js/jquery.js') }}"></script>
	<script src="{{ asset('js/plugins.min.js') }}"></script>
	<script src="{{ asset('js/functions.js') }}"></script>
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyDM1TZzvAIq5CzyPywHAMFmx-WUm7gsuQ0"></script>
    <script>
        const form = document.querySelector("#template-contactform")
        const submitButton = document.querySelector("#template-contactform-submit")
        const scriptURL = 'https://script.google.com/macros/s/AKfycbyILLSrmzEArru9Z9BSd1ZuQkht8AKNVZ1aE2Q9HRct9kuMCuFALUV54MKrQh0Hz2N9/exec'

        form.addEventListener('submit', e => {
			if (document.querySelector("#template-contactform-name").value == '' ||
				document.querySelector("#template-contactform-phone").value == '' ||
				document.querySelector("#template-contactform-message").value == '' ||
				document.querySelector("#template-contactform-email").value == '' ) {
				alert('Isi semua kolom!')
				return false;
			}
            submitButton.disabled = true
            e.preventDefault()
            let requestBody = new FormData(form)
            fetch(scriptURL, { method: 'POST', body: requestBody})
                .then(response => {
                alert('Success!', response)
                document.querySelector(".form-process").style.display = "none";
                form.reset();
                submitButton.disabled = false
            })
            .catch(error => {
                alert('Error!', error.message)
                document.querySelector(".form-process").style.display = "none";
                submitButton.disabled = false

                }
            )
        })
    </script>
</body>
</html>
